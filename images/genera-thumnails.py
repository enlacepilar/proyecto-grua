import os
from PIL import Image

cwd = os.getcwd()
print (cwd)
for archivo in os.listdir(cwd):
    if archivo.endswith('.jpg'):
        print (archivo)
        im = Image.open(archivo)
        im.thumbnail((500,375), Image.ANTIALIAS)
        nom = archivo.split ('.')[0]
        tnom = nom + "_thumb.jpg"
        im.save(tnom, "JPEG")
        print ('a href="%s" target="_blank"><img src= "%s"/></a><br>' % (archivo, tnom))
